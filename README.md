# laptop-setup

This repository sets up my laptop.

## Windows 10 host setup

Setup WSL2 https://docs.microsoft.com/en-us/windows/wsl/install-win10#manual-installation-steps

1. `dism.exe /online /enable-feature /featurename:Microsoft-Windows-Subsystem-Linux /all /norestart`
1. `dism.exe /online /enable-feature /featurename:VirtualMachinePlatform /all /norestart`
1. Install Kernel thingy https://aka.ms/wsl2kernel
1. `wsl --set-default-version 2`
1. Install [Ubuntu 20.04](https://www.microsoft.com/store/apps/9n6svws3rx71)
1. Install [Windows Terminal](https://docs.microsoft.com/en-us/windows/terminal/get-started)

TODO:
* Disable windows features - IE 10
* Can you automate setting up of the star menu and task bar?
* Uninstall unwanted programs
* Merge the scripts repo into this one
* Set up Packer hyperv-iso

## Ubuntu VM setup

```
ansible-playbook -K -i inventory main.yml
```

# Resources
neovim role - https://github.com/neovim/neovim/wiki/Installing-Neovim
